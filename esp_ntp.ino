#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "user_interface.h"
#include <Ticker.h>

Ticker ticker;

char ssid[] = ""; // your network SSID (name)
char pass[] = ""; // your network password

unsigned int localPort = 2390; // local port to listen for UDP packets

int ON_BOARD_LED = 2;
int LED          = D0;  // nodemcu LED (Inverted)
int PROBE_PIN    = D2;

/* Don't hardwire the IP address or we won't get the benefits of the pool.
    Lookup the IP address for the host name instead */
//IPAddress timeServer(129, 6, 15, 28); // time.nist.gov NTP server
IPAddress timeServerIP; // time.nist.gov NTP server address
//const char* ntpServerName = "172.17.2.123";
const char* ntpServerName = "ntp.is.co.za";
//const char* ntpServerName = "10.0.0.202";

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;

// pps timer object
os_timer_t ppsTimer;

bool loopTimerTicked = true;

// loop timer callback function
void loopTimerCB() {
  digitalWrite(LED, !(digitalRead(LED)));  //Toggle LED Pin
  //timer1_write(1000000);
  loopTimerTicked = true;
}

// pps timer callback function
void ppsTimerCB() {
  digitalWrite(ON_BOARD_LED, !(digitalRead(ON_BOARD_LED)));
  digitalWrite(PROBE_PIN,    !(digitalRead(PROBE_PIN)));
  Serial.println("Ticked");
  os_timer_arm(&ppsTimer, 1000, true);
}


void setup() {
  pinMode(LED, OUTPUT);
  pinMode(ON_BOARD_LED, OUTPUT);
  pinMode(PROBE_PIN, OUTPUT);
  digitalWrite(LED, LOW);
  digitalWrite(ON_BOARD_LED, HIGH);
  digitalWrite(PROBE_PIN, HIGH);

  Serial.begin(115200);
  Serial.println();
  Serial.println();

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(udp.localPort());

  // setup 10s loop to send an NTP request
  ticker.attach(10,loopTimerCB);

  // setup a timer to toggle a pin on a second boundary
  os_timer_setfn(&ppsTimer, (os_timer_func_t *)ppsTimerCB, (void *)0);
}


void loop() {
  if (loopTimerTicked == true) {
    loopTimerTicked = false;
    //get a random server from the pool
    WiFi.hostByName(ntpServerName, timeServerIP);
  
    sendNTPpacket(timeServerIP); // send an NTP packet to a time server
    // wait to see if a reply is available
    delay(500);
    digitalWrite(LED, !(digitalRead(LED)));

    int cb = udp.parsePacket();
    if (!cb) {
      Serial.println("no packet yet");
    } else {
      Serial.print("packet received, length=");
      Serial.println(cb);
      // We've received a packet, read the data from it
      udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
  
      //the timestamp starts at byte 40 of the received packet and is four bytes,
      // or two words, long. First, extract the two words:
  
      unsigned long highWord     = word(packetBuffer[40], packetBuffer[41]);
      unsigned long lowWord      = word(packetBuffer[42], packetBuffer[43]);
      unsigned long fracHighWord = word(packetBuffer[44], packetBuffer[45]);
      unsigned long fracLowWord  = word(packetBuffer[46], packetBuffer[47]);
      // combine the four bytes (two words) into a long integer
      // this is NTP time (seconds since Jan 1 1900):
      unsigned long secsSince1900     = highWord     << 16 | lowWord;
      unsigned long fracSecsSince1900 = fracHighWord << 16 | fracLowWord;
      Serial.print("Micro seconds : ");
      long microSecs = fracSecsSince1900 >> 12;
      Serial.println(microSecs);
      
      // set the 1 second timer to the next second boundary
      os_timer_arm(&ppsTimer, 1000-(microSecs/1000), true);
    }
  }
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress& address) {
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}